#!/usr/bin/env bash
# controller.sh
# controls the navigation of steps, testing the students code and using `tmux send-keys` to automate the transitioning as steps are completed.

while inotifywait -e close_write,delete_self -qq script.py; do
  bash .bin/test-students-code.sh
  ERR=$?
  if [ $ERR -eq 0 ]; then
    # success, proceed to next step
    sed -i "s/^  $lrnSTEP: TODO/  $lrnSTEP: Complete/" .lesson.yml
    git add script.py .lesson.yml; git commit -m "Success."
    sleep 1
    tmux send-keys -t lesson.2 Enter Enter "nice one!" Enter Enter
    oldSTEP=$lrnSTEP
    newSTEP=$((lrnSTEP+1))
    tmux set-environment lrnSTEP $newSTEP
    # @TODO not sure if we need shell env var, possibly just used tmux var
    export lrnSTEP=$newSTEP
    #tmux sendkeys export $lrnSTEP to some pane
    #tmux sendkeys export $lrnSTEP to another pane
    #tmux send-keys -t lesson.2 $newSTEP is "$newSTEP"
    sleep 1
    # does next step exist?
    if grep "  $newSTEP: TODO" .lesson.yml; then
      tmux send-keys -t lesson.2 "Next step is: number $newSTEP." Enter
      sleep 3
      git checkout -b step-$newSTEP
      cat .instructions/step-$newSTEP.md > instructions.md
      git add instructions.md; git commit -m "Update instructions for step $newSTEP."
      #tmux send-keys -t lesson.1 "./.bin/print-instructions.sh" Enter
      tmux send-keys -t lesson.1 ":vi" Enter
      # Instead of automatically clearing&reloading the run script, this should be handed off to the individual steps to perform, perhaps run a "completed-step-transition-#.sh" at this point instead. But for now, we should simply stop reloading the runner because it disrupts continuity between steps and that's sometimes quite problematic.
      #tmux send-keys -t lesson.2 C-c "clear" Enter ./.bin/run-students-code.sh Enter
    else
      tmux send-keys -t lesson.2 "Yay, congratulations, you've completed the lesson!"
      # what to do next:
      #   - submit data?
      #   - collect personal feedback?
      #   - start another lesson?
      #   - promote the open collective funding page? (once it exists)
    fi
  else
    # error. keep trying...
    case $ERR in
      121) ERROR="Syntax errors.";
        ;;
      122) ERROR=$(grep '@COMMIT' .tests/step-$lrnSTEP.sh | sed 's/^# @COMMIT: //');
        ;;
      123) ERROR="Output doesn't match.";
        ;;
    esac
    git add script.py
    git commit -m "$ERROR"
    tmux send-keys -t lesson.2 Enter Enter "not quite. keep trying..." Enter Enter
  fi
done
