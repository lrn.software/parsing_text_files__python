#!/usr/bin/env bash

#set -eo pipefail


# CHECK FOR SYNTAX ERRORS
pylint -E script.py > /dev/null
if [[ $? -eq 0 ]]; then
  echo 'syntax passes' > /dev/null
else
  exit 121
fi

# CHECK STEP-SPECIFIC CONDITIONS
if [[ -f .tests/step-$lrnSTEP.sh ]]; then
  ./.tests/step-$lrnSTEP.sh
  if [ $? -eq 0 ]; then
    STATUS='pass'
  else
    # error
    exit 122
  fi
fi


# CHECK OUTPUT OF SCRIPT
if [[ ! -d /tmp/learnCode/tests/$lrnLESSON/$lrnSTEP ]]; then
  mkdir -p /tmp/learnCode/tests/$lrnLESSON/$lrnSTEP
fi
python .tests/step-$lrnSTEP.py tech > /tmp/learnCode/tests/$lrnLESSON/$lrnSTEP/rubrik.txt
python script.py tech > /tmp/learnCode/tests/$lrnLESSON/$lrnSTEP/student.txt
cd /tmp/learnCode/tests/$lrnLESSON/$lrnSTEP
diffTEST=$(diff -q rubrik.txt student.txt)
# @ASK why does ok work but error doesn't????
# because of `set -eo pipefail`
if [[ "$diffTEST" =~ .*"differ".* ]]; then
  # error. return code for controller.sh to handle
  exit 123
else
  exit 0
fi
