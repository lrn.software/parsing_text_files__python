#!/bin/bash
clear
echo "Each time you save script.py this pane will run:
  python script.py tech
to execute the script with 'tech' supplied as an argument.
"

while inotifywait -e modify -qq script.py; do
  python script.py tech
done
