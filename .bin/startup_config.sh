#!/usr/bin/env bash

#set -euo pipefail

export lrnLESSON=$(echo $PWD | sed 's|.*/\(\w*\)$|\1|')
export lrnSTEP=$(grep TODO .lesson.yml | head -n 1 | grep -oe '[0-9]*')
#doing git operations in multiple panes simultaneously can result in lockfile errors
#git checkout step-$lrnSTEP
tmux set-environment lrnSTEP $lrnSTEP
tmux set-environment lrnLESSON $lrnLESSON
