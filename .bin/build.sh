#!/usr/bin/env bash
# BUILD SCRIPT: used only for *developing* the lesson, not used during the student's learning.

# @TODO generate the list of step#s in .lesson.yml programmatically, do this before running function `make_readme`.

#git checkout master

# delete existing step-* branches
delete_branches () {
  if git branch | grep -q 'step-'; then
    # this will delete all student work
    for b in $(git branch | grep -e 'step-[0-9]*' | sort -r -n -t- -k2); do
      git branch -D $b
    done
  fi
}

# re-create step-* branches
create_branches () {
  for s in $(ls .tests | grep -e '\.py$' | sort -n -t- -k2); do
    step=$(echo $s | sed 's/\.py//')
    git checkout -b $step
    cat .instructions/$step.md > instructions.md
    git add instructions.md
    git commit -m "$step"
  done
  git checkout master
}

# Create README file based on .lesson.yml
make_readme () {
  rdme () { echo "$1" >> README.md; }
  nwl () { echo >> README.md; }
  # @TODO load all values from .lesson.yml as variables through something more graceful than a bunch of individual pipelines
  lesson=$(head -n 1 .lesson.yml | sed 's/Course_Name: //')
  description=$(grep -E '^Description:' .lesson.yml | sed 's/Description: //')
  steps=$(grep '[0-9]: TODO' .lesson.yml | tail -n 1 | grep -o '[0-9]*')
  #deps=$(sed '0,/Software_Dependencies/d' .lesson.yml | sed 's/  - //')
  githttp=$(grep 'Git_Repo_URL' .lesson.yml | sed 's/Git_Repo_URL: //' | sed 's/$/.git/')
  gitgit=$(echo $githttp | sed 's|https://|git@|' | sed 's|.com/|.com:|')
  rm README.md
  #echo "# Lesson README file" > README.md; nwl
  rdme "# $lesson"; nwl
  rdme "This is a README file. Every open source project has one. Always read the README file."; nwl
  rdme "Here in this lesson, in $steps steps you'll $description."; nwl; nwl
  rdme "### Getting started"; nwl
  rdme "Install \`lrn\` via https://gitlab.com/lrn.software/lrn_installer."; nwl
  rdme "That will download this lesson."
  rdme "Once you have \`lrn\` installed, run \`lrn\` from your terminal to begin a learning session."
}

make_readme

# delete branches containing "student" work
delete_branches

# sometimes we want to see how the branches will look without having to manually go through all the steps, in that case uncomment the following line
#create_branches

# executability keeps being revoked on scripts. perhaps git filemode?
chmod -R +x .bin .tests
