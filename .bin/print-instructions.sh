#!/bin/bash
# oneliner for printing instructions
# since tools like `print` and `echo` and `cat` break up words when wrapping text to the next line, lets measure the column width of the terminal window and then use `fold` to wrap lines neatly according to spaces between words.
# less has a glaring filename that distracts from the instructions and more just dumps into terminal
# @TODO a more elegant presentation of instruction text is needed
clear
#cols=$(tput cols) && echo $(cat instructions.md) | fold -w $cols -s
vim -u .config/pager.vimrc instructions.md
