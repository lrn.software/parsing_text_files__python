#!/usr/bin/env bash

export lrnSTEP=$(grep TODO .lesson.yml | head -n 1 | grep -oe '[0-9]*')
git checkout -b step-$lrnSTEP
# @TODO: add condition, only do this when either: 1) diff between files, or 2) git commit/branch doesn't exist
cat .instructions/step-$lrnSTEP.md > instructions.md
git add instructions.md; git commit -m "Update instructions for step $lrnSTEP."
