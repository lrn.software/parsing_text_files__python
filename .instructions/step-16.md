We need to keep track of the fact that we've entered a tagged section of content.

- create a `copy` boolean and set it to be `False` initially, do this on a new line right above the inner `for` loop's declaration.
- on a new line after the `print()` statement and in the same logic group (having the same indentation), make the boolean true.
- use this boolean to determine whether or not tagged content should be printed, inside a new logic group after `if tagName...` create `elif copy:` and inside that do `print(line)`

