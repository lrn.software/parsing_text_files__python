Now that we've got the files in proper order we can open them and start working with their contents, like `with open ('path/to/file.md') as file:`.

When you do this, pass to `open()` the `directory` variable and `filename` variable *concatenated* together (using string concatenation operator).

Then with the open file use `file.readline()` to `print()` the first line of the file.
