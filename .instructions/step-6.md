The `os` module provides a `.listdir()` method.

Invoke that, passing `directory` to it as an argument, and print the results.
