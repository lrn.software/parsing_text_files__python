We need to stop the script from printing when it reaches the end of the tagged content.  That end is determined by any of these three regex patterns `^##|^---|^:\w*:`.

- make a new `if` statement inside `elif` using `re.search()` to evaluate the current line and only print the line if we're *not* on a line matching that regex
- create an `else` for this if and inside it flip the boolean switch, `copy` should now be false.
- additionaly, remove `\n` character from `line` here as we've done twice before
