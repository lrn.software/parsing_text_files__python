Notice how the results were not in order.  We want the report to order them by date.  Use the `sorted()` function to sort the files in `directory`.
