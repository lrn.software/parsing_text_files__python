Add the line number that the tag appeared at to the report:

- initalize a variable `lnum` equal to `1` above the `for line in file:` line
- then at the very beginning of that `for` increment this integer by 1
- add `lnum` after to the end of the `print()` statement, note that since `lnum` is an integer and integers can't be concatenated to strings so the `str()` function will need to be used
