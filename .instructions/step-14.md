Now we want to clean that up a little...

- use the `string.replace()` on `line`, as was done on `date` previously, here where `line` is being assigned to `section`
- run a regex substition to modify `section` (this looks like defining the `section` variable again) assigning `section` the value of `re.sub('#{1,} ', '*')`. 
- create another assigment statement like that but use string concatenation to append `'* '` to the end of `section`.
- add a space, `' '`, between `date` and `section` in the `print() statement
