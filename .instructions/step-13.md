The tagged content can appear in different sections within the journal entry so we'll use regex to find the section before we find the tag.

- import the `re` module for regex
- start a new `if` evaluating the regex search method, `re.search()` takes two arguments, the first is the regex itself `r'^#{2,} \w{1,}'` to match the section titles, and the second is the variable we're looking through, `line` in this case.
- when a section title is found, assign it (the line) to a new variable called `section` and add this to the `print()` function following `date`.
