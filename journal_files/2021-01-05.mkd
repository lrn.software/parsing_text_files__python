# Tuesday 05 January 2021
-------------------------
[Diary template](../../../code/bin/vimwiki-diary-template.py)
[Journal](journal.mkd)
[groceries and stuff](groceries and stuff)

## Dreams, Waking, Morning


## TODO
- get up | 8ish in hammock in woods <3
    no backpain, peace in nature, but a bit cold without sleeping pad
- [x] tea/breakfast journal

- banking, transfer$
:tech:
- wanting a `myls()` function
- troubleshoot shell startup performance and find `nvm` to be the culprit
- drupal project browser notes
---

**Care**
- [ ] body self care, deep relaxtion/nourishment
- [x] find solace in nature
- [ ] meditate

- [x] 06:00 PM-06:45 PM zoom w/people

**Night**
- sleep @

--------

## Notes

### Reflections
in the woods, in the dark with a headlamp on, looking at my computer

### Struggles
:tech:
it makes me irrate when digital service companies (github, pantheon, digitalocean, etc) don't have responsive website interfaces!

### Accomplishments

### Gratitudes

### Forgiveness

## Tomorrow
do stuff
