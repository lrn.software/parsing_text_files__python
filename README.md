# Intro to programming with Python: parsing text files for a report

This is a README file. Every open source project has one. Always read the README file.

Here in this lesson, in 19 steps you'll parse through a directory of text files looking for a string, and generating a report of the contents following the matched string.


### Getting started

Install `lrn` via https://gitlab.com/lrn.software/lrn_installer.

That will download this lesson.
Once you have `lrn` installed, run `lrn` from your terminal to begin a learning session.
