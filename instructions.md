This is a lesson for entry level programming: use the `python` scripting language to parse through text files.

Use the `lrn` command to launch the learning environment and begin the lesson.
