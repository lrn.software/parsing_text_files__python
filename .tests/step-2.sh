#!/usr/bin/env bash


grep -q "tagName = sys.argv\[1\]" script.py
if [ $? -eq 0 ]; then
  exit 0
else
  exit 123
fi

# @HINT: hopefully just doing what the instructions say is sufficient for this step...
# @COMMIT: Missing/incorrect definition of tagName variable.
