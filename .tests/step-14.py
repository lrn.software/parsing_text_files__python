#!/usr/bin/python3

import sys
import os
import re

tagName = ':' + sys.argv[1] + ':'
directory = os.getcwd() + '/journal_files/'

for filename in sorted(os.listdir(directory)):
    with open(directory + filename) as file:
        date = file.readline()
        date = date.replace("\n", "")
        for line in file:
            if re.search(r'^#{2,} \w{1,}', line):
                section = line.replace("\n", "")
                section = re.sub('#{1,} ', '*', section)
                section = section + '* '
            if tagName in line:
                print('\n' + date + ' ' + section)
