#!/usr/bin/python3

import sys
import os

tagName = ':' + sys.argv[1] + ':'
directory = os.getcwd() + '/journal_files/'

for filename in sorted(os.listdir(directory)):
    with open(directory + filename) as file:
        date = file.readline()
        date = date.replace("\n", "")
        print(date)
