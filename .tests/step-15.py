#!/usr/bin/python3

import sys
import os
import re

tagName = ':' + sys.argv[1] + ':'
directory = os.getcwd() + '/journal_files/'

for filename in sorted(os.listdir(directory)):
    with open(directory + filename) as file:
        date = file.readline()
        date = date.replace("\n", "")
        lnum =1
        for line in file:
            lnum += 1
            if re.search(r'^#{2,} \w{1,}', line):
                section = line.replace("\n", "")
                section = re.sub('#{1,} ', '*', section)
                section = section + '* '
            if tagName in line:
                print('\n' + date + ' ' + section + str(lnum))
                #print('\n' + date + ' ' + section + line.rstrip() + ' line#' + str(lnum))
